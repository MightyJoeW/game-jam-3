extends ProgressBar

const state = preload("res://stateEnum.gd").state

export var game_length_in_minutes = 5
var actual_minute = 30
var timer = Timer.new()
var lastTime
var currentState
var nextTimeToSpawn
var firstSpawn
var totalTime

# Called when the node enters the scene tree for the first time.
func _ready():
	firstSpawn = true
	totalTime = game_length_in_minutes * actual_minute
	#Timer config
	timer.connect("timeout", self, "_on_timer_timeout") 
	timer.set_one_shot(true)
	timer.set_timer_process_mode(Timer.TIMER_PROCESS_IDLE)
	timer.set_wait_time(totalTime)
	#This connects the timer to the Progress Bar which will actually start the timer
	add_child(timer)
	
	#Use this to calculate progress bar value
	lastTime = int(timer.time_left)
	max_value = totalTime
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	if(lastTime != int(timer.time_left)):
#		print('time remaining: ', lastTime)
#		print('progress bar value: ', value)
		value = totalTime - lastTime
		lastTime = int(timer.time_left)
		
	if(lastTime == nextTimeToSpawn && currentState != state.talking && currentState != state.instructions):
		if(firstSpawn):
			firstSpawn = false
		else: 
#			print(lastTime, ' seconds have passed')
			emit_signal('npc_encounter')
	
func _on_timer_timeout():
	emit_signal('game_over_by_clock')
	pass

func _state_change(signalCurrentState):
	currentState = signalCurrentState
	pass # Replace with function body.
	
func _on_instructions_instructions_end():
	nextTimeToSpawn = (totalTime) - 10
#	print('progress bar max value: ', self.max_value)
#	print('timer time left: ', timer.time_left)
	timer.start()

func _on_DialougeBox_dialogue_finished():
	nextTimeToSpawn = lastTime - _random_range(10, 15)
#	print('nextTimeToSpawn: ', nextTimeToSpawn)
	pass # Replace with function body.
	
func _random_range(min_range, max_range):
	randomize()
	return range(min_range, max_range)[randi()%range(min_range, max_range).size()]

signal npc_encounter
signal game_over_by_clock