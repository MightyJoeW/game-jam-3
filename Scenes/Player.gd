extends AnimatedSprite

const state = preload("res://stateEnum.gd").state
# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	play('typing')
	pass # Replace with function body.

func _on_StateMachine__state_change(signalCurrentState):
	if(signalCurrentState == state.talking):
		stop()
	elif(signalCurrentState == state.working):
		play('typing')
	pass # Replace with function body.
