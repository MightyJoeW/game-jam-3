extends ProgressBar

const state = preload("res://stateEnum.gd").state

export var maxLimit = 1000
var currentState
var isFull

# Called when the node enters the scene tree for the first time.
func _ready():
#	print('currentState in WorkUI: ', currentState)
	self.max_value = maxLimit
	isFull = false
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	if(isFull):
		emit_signal('game_win')
	pass

func _input(event):
#	if(event is InputEventKey && event.is_pressed()):
	if(currentState == state.working):
		if(!isFull):
			if(event is InputEventKey && Input.is_action_just_pressed('work')):
				self.value += 1
	#			print(event)
				if(self.value >= maxLimit):
					isFull = true
	pass
	
func _state_change (signalCurrentState):
	currentState = signalCurrentState
#	print('WorkUI state: ', currentState)
	pass
	
signal game_win