extends NinePatchRect

const state = preload("res://stateEnum.gd").state
const Character = preload("res://Character.gd")

#var characterNames = ['Karen', 'Jim', 'Dwight', 'Micheal']
var characters = [
	Character.new(
		'Pam', 
		0.05, 
		'res://Resources/Dialouge/talk_female.wav', 
		'res://Resources/NPC/coworker-18.png',
		[
			'Knock knock! Got a sec?', 
			'Ugh, is it Friday yet?',
			'',
			'Nope still not Friday'
		]),
	Character.new(
		'Edgar', 
		0.07, 
		'res://Resources/Dialouge/talk_male.wav', 
		'res://Resources/NPC/coworker-17.png',
		[
			'So my pet rat had babies this weekend.',
			'It took a few hours, but my mom and I named them all.',
			'There\'s Barney, Ramona, Anastasia...',
			'Fitzgerald, Regular Gerald, John Boy...',
			'Robin Williams, Jessica Rabbit...',
			'And my favorite, Don Quixote!'
		]),
	Character.new(
		'Nicheal', 
		0.03, 
		'res://Resources/Dialouge/talk_female.wav', 
		'res://Resources/NPC/coworker-19.png',
		[
			'This place has a got a real morale problem.',
			'Everyone mostly cries.',
		]),
	Character.new(
		'Jim', 
		0.05, 
		'res://Resources/Dialouge/talk_male.wav', 
		'res://Resources/NPC/test_portrait.png',
		[
			'I\'ts 5 o\'clock somewhere.'
		])
]

var dialogueBox
var nameTextBox
var nameBox
var currentState
var currentCharacterIndex

func _ready():
	dialogueBox = get_node('NPCDialogue')
	nameTextBox = get_node('NPCName')
#	print(nameBox)
	nameBox.hide()
	
	currentCharacterIndex = 0
#	dialogueBox.add_text('override')
#	nameBox.text = 'Kent'

func _process(_delta):
	if(Input.is_action_just_pressed('ui_focus_next') && currentState == state.talking):
		emit_signal('dialogue_finished')
	pass

func _state_change(signalCurrentState):
	nameBox = get_node('../NameBox')
	currentState = signalCurrentState
	if(signalCurrentState == state.talking):
#		print('characterName: ', characters[currentCharacterIndex].get_character_name())
#		print('currentCharacterIndex: ', currentCharacterIndex)
		if(currentCharacterIndex != (characters.size())):
			nameTextBox.text = characters[currentCharacterIndex].get_character_name()
			match currentCharacterIndex:
				0:
					get_child(2).show()
				1:
					get_child(3).show()
				2:
					get_child(1).show()
				3:
					get_child(4).show()
			self.show()
			nameBox.show()
			emit_signal('dialouge_start', characters[currentCharacterIndex].get_character_speed(), characters[currentCharacterIndex].get_character_noise(), characters[currentCharacterIndex].get_character_dialogue())
		else:
			emit_signal('stop_spawning_mini_games')
			emit_signal('dialogue_finished')
	else:
		get_child(1).hide()
		get_child(2).hide()
		get_child(3).hide()
		get_child(4).hide()
		nameBox.hide()
		self.hide()

func _on_NPCDialogue_dialogue_end():
	emit_signal('dialogue_finished')
	currentCharacterIndex += 1
	pass # Replace with function body.

signal dialouge_start()
signal dialogue_finished()
signal stop_spawning_mini_games()