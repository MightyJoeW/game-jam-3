extends Node

const state = preload("res://stateEnum.gd").state

var miniGameScene = load('res://Scenes/MiniGameManager.tscn')

var currentState
var shouldSpawnMiniGames

# Called when the node enters the scene tree for the first time.
func _ready():
	shouldSpawnMiniGames = true
	currentState = state.instructions
	emit_signal('_state_change', currentState)

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(_delta):
#	if(Input.is_action_just_pressed('ui_focus_next')):
#		_switch_state()

func _switch_state():
	if(currentState == state.working):
		if(shouldSpawnMiniGames):
			var miniGameSceneInstance = miniGameScene.instance()
			miniGameSceneInstance.set_name('MiniGame')
			miniGameSceneInstance.connect('_pass_grade', get_node('../DialougeBox/NPCDialogue'), '_on_pass_grade')
			add_child(miniGameSceneInstance)
			currentState = state.talking
	elif(currentState == state.talking):
		currentState = state.working
	elif(currentState == state.instructions):
		currentState = state.working
#	print('_switch_state(): ', state.keys()[currentState])
	emit_signal('_state_change', currentState)
	
func _npc_encounter():
	_switch_state()
	
func _dialogue_finished():
#	print('dialogue finished')
	_switch_state()

func _on_spawn_another_game():
	var miniGameSceneInstance = miniGameScene.instance()
	miniGameSceneInstance.set_name('MiniGame')
	miniGameSceneInstance.connect('_pass_grade', get_node('../DialougeBox/NPCDialogue'), '_on_pass_grade')
	add_child(miniGameSceneInstance)
	pass # Replace with function body.

signal _state_change(signalCurrentState)

func _on_DialougeBox_stop_spawning_mini_games():
	shouldSpawnMiniGames = false
	pass # Replace with function body.

func _on_instructions_instructions_end():
	_switch_state()

func _on_TimeUI_game_over_by_clock():
#	print(get_node('../GameLose'))
	if(get_child(0) != null):
		remove_child(get_child(0))
	if(get_node('../DialougeBox') != null):
		get_parent().remove_child(get_node('../DialougeBox'))
	if(get_node('../NameBox') != null):
		get_parent().remove_child(get_node('../NameBox'))
	get_parent().remove_child(get_node('../CanvasLayer'))
	get_node('../GameLose').show()
	get_parent().remove_child(get_node('../CanvasLayer'))
	currentState = state.gameover
	pass # Replace with function body.

func _on_WorkUI_game_win():
#	print('game win')
	get_parent().remove_child(get_node('../CanvasLayer'))
	emit_signal("_game_over")
	get_node('../GameWin').show()
	currentState = state.gameover
	pass # Replace with function body.
	
