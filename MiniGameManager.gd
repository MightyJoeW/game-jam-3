extends Node2D

var arrowScene = load('res://Scenes/Arrow.tscn')
var timer = Timer.new()
var timesToSpawn
var spawnIndex
var grades = []

func _ready():
	timesToSpawn = _random_range(10, 13)
	
#	print(timesToSpawn)
	spawnIndex = 0
	var arrowSceneInstance = arrowScene.instance()
	arrowSceneInstance.set_name('arrow_' + str(spawnIndex))
	arrowSceneInstance.connect('report_grade', self, '_on_report_grade')
	add_child(arrowSceneInstance)
	
	timer.connect("timeout", self, "_on_timer_timeout") 
	timer.set_one_shot(true)
	timer.set_timer_process_mode(Timer.TIMER_PROCESS_IDLE)
	timer.set_wait_time(.5)
	#This connects the timer to the Progress Bar which will actually start the timer
	add_child(timer)
	timer.start()
#	print(timer)

func _process(_delta):
	if(spawnIndex == timesToSpawn):
		if(!has_node('arrow_' + str(spawnIndex - 1))):
			_calculate_grade()
			get_parent().remove_child(self)
			pass
	pass
	
func _on_timer_timeout():
	spawnIndex += 1
	if(spawnIndex < timesToSpawn):
		var arrowSceneInstance = arrowScene.instance()
		arrowSceneInstance.set_name('arrow_' + str(spawnIndex))
		arrowSceneInstance.connect('report_grade', self, '_on_report_grade')
		add_child(arrowSceneInstance)
		timer.start()
	pass
	
func _calculate_grade():
	var totalPoints = 0.0
	var score = 0
	for i in grades.size():
		totalPoints += grades[i]
	score = int(100 * (totalPoints / (float(grades.size()))))
	emit_signal('_pass_grade', score)
	
func _random_range(min_range, max_range):
	randomize()
	return range(min_range, max_range)[randi()%range(min_range, max_range).size()]
	
func _on_report_grade(grade):
	grades.append(grade)
	
signal _pass_grade(grade)