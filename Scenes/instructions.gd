extends Sprite

func _ready():
	self.show()

func _process(_delta):
	if(Input.is_action_just_pressed("work") || Input.is_action_just_pressed("ui_accept")):
		emit_signal('instructions_end')
		get_parent().remove_child(self)

signal instructions_end