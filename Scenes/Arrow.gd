extends KinematicBody2D

var spriteArray = [
	'res://Resources/MiniGame/arrow_down.png',
	'res://Resources/MiniGame/arrow_up.png',
	'res://Resources/MiniGame/arrow_left.png',
	'res://Resources/MiniGame/arrow_right.png'
]
var spriteGreatArray = [
	'res://Resources/MiniGame/arrow_down_great.png',
	'res://Resources/MiniGame/arrow_up_great.png',
	'res://Resources/MiniGame/arrow_left_great.png',
	'res://Resources/MiniGame/arrow_right_great.png'
]

var testAlpha = 0.0
var speed = 225
var isPressed = false

var arrowSprite = Sprite.new()
var spriteIndex

func _ready():
	self.position = Vector2(920, 340)
	spriteIndex = _random_sprite()
#	print(spriteIndex)
	arrowSprite.texture = load(spriteArray[spriteIndex])
	arrowSprite.set_name('arrowSprite')
	add_child(arrowSprite)
	
func _process(_delta):
	if(self.position.x > 340):
		_check_for_input()
		_move()
	else:
		if(!isPressed):
			_grade()
		get_parent().remove_child(self)
	pass
	
func _check_for_input():
	if(self.position.x < 580 && self.position.x > 440):
		if(Input.is_action_just_pressed('ui_down') && spriteIndex == 0):
			_grade()
		if(Input.is_action_just_pressed('ui_up') && spriteIndex == 1):
			_grade()
		if(Input.is_action_just_pressed('ui_left') && spriteIndex == 2):
			_grade()
		if(Input.is_action_just_pressed('ui_right') && spriteIndex == 3):
			_grade()
	pass
	
func _move():
	if(self.position.x > 500):
		if(testAlpha < 1):
			testAlpha += 0.025
	if(self.position.x < 440):
		if(testAlpha > 0):
			testAlpha -= 0.025
	self.modulate.a = testAlpha
	move_and_slide(Vector2(-speed, 0))

func _grade():
	if(!isPressed):
		if(self.position.x < 550 && self.position.x > 470):
			arrowSprite.texture = load(spriteGreatArray[spriteIndex])
			emit_signal('report_grade', 1)
			isPressed = true
	#		print('great')
		else:
			emit_signal('report_grade', 0)
	#		print('bad')
	
func _random_sprite():
	randomize()
	return randi()%4
	
signal report_grade()