extends RichTextLabel

var dialogue = []
var dialogueFail = ['Are you even listening?', 'Pay attention!', 'Listen to me!']
var dialogueIndex
var timer = Timer.new()
var audioPlayer = AudioStreamPlayer.new()

func _ready():
	timer.connect("timeout", self, "_on_timer_timeout") 
	timer.set_one_shot(true)
	timer.set_timer_process_mode(Timer.TIMER_PROCESS_IDLE)
#	print('audioPlayer.volume_db: ', audioPlayer.volume_db)
	audioPlayer.volume_db = -20.0
	pass

func _on_timer_timeout():
#	print('self.get_total_character_count: ', self.get_total_character_count())
#	print('self.get_visible_line_count: ', self.get_visible_characters())
#	print('current character: ', dialogue[dialogueIndex][self.get_visible_characters() - 1])
	if(self.get_total_character_count() != self.get_visible_characters()):
		timer.start()
		if(dialogue[dialogueIndex][self.get_visible_characters() - 1] != ' '):
			audioPlayer.play()
		self.set_visible_characters(self.get_visible_characters()+1)

func _on_DialougeBox_dialouge_start(talkSpeed, talkNoisePath, characterDialogue):
	audioPlayer.stream = load(talkNoisePath)
	self.add_child(audioPlayer)
	
	timer.set_wait_time(talkSpeed)
	self.add_child(timer)
	
	dialogue = characterDialogue
	dialogueIndex = 0
	_next_dialogue()
	pass # Replace with function body.
	
func _next_dialogue():
#	print('called from next')
	if(dialogueIndex < dialogue.size()):
		
		timer.start()
		audioPlayer.play()
		
		self.set_visible_characters(0)
		self.set_bbcode(dialogue[dialogueIndex])
	else:
		self.remove_child(audioPlayer)
		self.remove_child(timer)
		emit_signal('_dialogue_end')
	pass
	
func _on_pass_grade(grade):
	if(self.get_total_character_count() == self.get_visible_characters()):
#		print('dialogueIndex: ', dialogueIndex)
#		print('dialogue.size(): ', dialogue.size() - 1)
		if(dialogueIndex < dialogue.size()):
			if(grade < 70):
				randomize()
				dialogue.insert(dialogueIndex + 1, dialogueFail[randi()%3])
#				print(dialogue)
				dialogueIndex += 1
				emit_signal('_spawn_another_game')
			else:
				if(dialogueIndex != (dialogue.size() - 1)):
					emit_signal('_spawn_another_game')
				dialogueIndex += 1
			_next_dialogue()
			
	print('grade: ', grade)

signal _dialogue_end()
signal _spawn_another_game()