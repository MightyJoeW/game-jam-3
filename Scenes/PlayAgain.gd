extends NinePatchRect

var isGameOver

func _ready():
	isGameOver = false

func _input(event):
	if(get_parent().is_visible_in_tree()):
		if(event is InputEventKey && Input.is_action_just_pressed('ui_accept')):
			get_tree().reload_current_scene()
			