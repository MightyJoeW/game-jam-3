extends Node

var characterName
var talkSpeed
var talkNoisePath
var imagePath
var dialogue

func _init(characterName, talkSpeed, talkNoisePath, imagePath, dialogue):
	self.characterName = characterName
	self.talkSpeed = talkSpeed
	self.talkNoisePath = talkNoisePath
	self.imagePath = imagePath
	self.dialogue = dialogue
	
func get_character_name():
	return self.characterName
	
func get_character_speed():
	return self.talkSpeed
	
func get_character_noise():
	return self.talkNoisePath
	
func get_character_image():
	return self.imagePath
	
func get_character_dialogue():
	return self.dialogue